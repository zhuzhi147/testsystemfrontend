import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MathTopicComponent } from "./components/teacher/math-topic/math-topic.component";
import { LoginFormComponent } from "./components/login-form/login-form.component";
import { NewPasswordComponent } from "./components/new_password/new-password/new-password.component";
import { MathTestComponent } from "./components/teacher/math-test/math-test.component";
import { MenuComponent } from "./components/teacher/menu/menu.component";
import { RegisterComponent } from "./components/register/register.component";
import { AuthGuard } from "./guard/auth.guard";
import { StudentChoiceComponent } from "./components/student/student-choice/student-choice.component";
import { StudentsPageComponent } from "./components/student/students-page/students-page.component";
import { StudentTestComponent } from "./components/student/student-test/student-test.component";

const routes: Routes = [
  {
    path: "",
    component: LoginFormComponent
  },

  {
    path: "",
    canActivate: [AuthGuard],
    data: { expectedRole: "student" },
    children: [
      {
        path: "student",
        component: StudentsPageComponent
      },
      {
        path: "student/:id",
        component: StudentChoiceComponent
      },
      {
        path: "student-test/:id",
        component: StudentTestComponent
      }
    ]
  },
  {
    path: "login",
    component: LoginFormComponent
  },
  {
    path: "topics",
    component: MathTopicComponent,
    canActivate: [AuthGuard],
    data: { expectedRole: "teacher" }
  },
  {
    path: "test",
    component: MathTestComponent,
    canActivate: [AuthGuard],
    data: { expectedRole: "teacher" }
  },
  {
    path: "newpassword",
    component: NewPasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "menu",
    component: MenuComponent,
    canActivate: [AuthGuard],
    data: { expectedRole: "teacher" }
  },
  {
    path: "register",
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  LoginFormComponent,
  MathTopicComponent,
  MathTestComponent,
  NewPasswordComponent,
  MenuComponent,
  RegisterComponent,
  StudentChoiceComponent,
  StudentsPageComponent,
  StudentTestComponent
];
