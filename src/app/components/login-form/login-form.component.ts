import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { User } from "src/app/services/models/user/user";
import { ToastrService } from "ngx-toastr";
import { HttpErrorResponse } from "@angular/common/http";
import { PwaService } from "src/app/services/pwa.service";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.css"]
})
export class LoginFormComponent implements OnInit {
  user: User;
  constructor(
    private router: Router,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.user = new User();
  }

  ngOnInit() {}

  public login(user: User): void {
    this.userService.login(user).subscribe(
      data => {
        if (data.accessToken) {
          localStorage.setItem("access_token", data.accessToken);
          if (data.type === "teacher") {
            this.router.navigateByUrl("/menu");
          } else {
            this.router.navigateByUrl("/student");
          }
        }
      },
      (error: HttpErrorResponse) =>
        this.toastr.error(error.error.message, "Грешка!")
    );
  }

  newProfile() {
    this.router.navigate(["/register"]);
  }
}
