import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { User } from "src/app/services/models/user/user";
import { ToastrService } from "ngx-toastr";
import { Teacher } from "src/app/services/models/user/teacher";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  user: User;

  constructor(private userService: UserService, private toastr: ToastrService) {
    this.user = new User();
  }

  ngOnInit() {}

  public register(user: User): void {
    if (user.type === "teacher") {
      this.user.teacher = new Teacher();
      this.user.teacher.name = this.user.name;
    } else {
      delete this.user.teacher;
    }

    this.userService.register(user).subscribe(data => {
      if (data) {
        this.toastr.success("Успешно създаден потребител!", "Регистрация!");
      }
    });
  }
}
