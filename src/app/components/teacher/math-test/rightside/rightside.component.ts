import { Component, OnInit, Input } from "@angular/core";
import { MathTest } from "src/app/services/models/mathtest";
import { MathSubTopic } from "src/app/services/models/subtopics";
import { MathtestService } from "src/app/services/mathtest.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-rightside",
  templateUrl: "./rightside.component.html",
  styleUrls: ["./rightside.component.css"]
})
export class RightsideComponent implements OnInit {
  @Input() subtopicId: number;

  public showData = false;
  public mathTest: MathTest;

  constructor(
    private mathTestService: MathtestService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.mathTest = new MathTest();
    this.mathTest.mathSubTopic = new MathSubTopic();
    this.mathTest.mathTasks = [];
  }

  addTest(): void {
    this.mathTest.mathTasks.push({
      task: "",
      image: "",
      answers: "",
      correctAnswer: ""
    });
  }

  onSubmit(mathTest: MathTest) {
    mathTest.mathSubTopic.id = this.subtopicId;
    this.mathTestService.createTest(mathTest).subscribe(res => {
      if (res) {
        this.toastr.success("Успешно записахте тест!", "Тест");
      }
    });
  }

  newTest() {
    this.router.navigate(["/test"]);
  }
}
