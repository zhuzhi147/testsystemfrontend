import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MathtopicService } from "src/app/services/mathtopic.service";
import { MathTopic } from "src/app/services/models/topics";
import { SubtopicService } from "src/app/services/subtopic.service";
import { MathSubTopic } from "src/app/services/models/subtopics";
import { Router } from "@angular/router";

@Component({
  selector: "app-leftside",
  templateUrl: "./leftside.component.html",
  styleUrls: ["./leftside.component.css"]
})
export class LeftsideComponent implements OnInit {
  @Output() showDataEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output() subtopicId: EventEmitter<number> = new EventEmitter();

  mathTopics: MathTopic[] = [];
  mathSubtopics: MathSubTopic[] = [];
  public classes = ["5", "6"];

  mathTopic: MathTopic;
  mathSubtopic: MathSubTopic;

  subtopicSelectDisabled: boolean = true;
  mathtopicsSelectedDisabled: boolean = true;

  constructor(
    private mathTopicService: MathtopicService,
    private mathSubtopicService: SubtopicService,
    private router: Router
  ) {
    this.mathTopic = new MathTopic();
    this.mathSubtopic = new MathSubTopic();
  }

  ngOnInit(): void {}

  show(): void {
    this.showDataEmitter.emit(true);
  }

  getSubtopicsByTopicId(id: string): void {
    this.mathSubtopicService.getSubtopicByTopicId(id).subscribe(subtopics => {
      if (subtopics) {
        this.mathSubtopics = subtopics;
        this.subtopicSelectDisabled = false;
      }
    });
  }

  topicSelectChange(mathTopicId: string): void {
    this.getSubtopicsByTopicId(mathTopicId);
  }

  getMathtopicsByClass(classSelected: string): void {
    this.mathTopicService
      .getMathTopicsByClass(classSelected)
      .subscribe(topics => {
        if (topics) {
          this.mathTopics = topics;
          this.mathtopicsSelectedDisabled = false;
        }
      });
  }

  topicSelectChangeByClass(classSelected: string): void {
    this.getMathtopicsByClass(classSelected);
  }

  subtopicSelectChange(subtopicId: number) {
    this.subtopicId.emit(subtopicId);
  }

  previosPage() {
    this.router.navigate(["/menu"]);
  }
}
