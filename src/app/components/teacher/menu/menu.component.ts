import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit {
  constructor(private router: Router, private userService: UserService) {}

  ngOnInit() {}

  goToMathTopic() {
    this.router.navigate(["/topics"]);
  }

  goToMathTest() {
    this.router.navigate(["/test"]);
  }

  logout(): void {
    this.userService.logout(this.router);
  }
}
