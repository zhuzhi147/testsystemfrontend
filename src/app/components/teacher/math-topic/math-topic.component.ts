import { Component, OnInit } from "@angular/core";
import { MathTopic } from "src/app/services/models/topics";
import { MathtopicService } from "src/app/services/mathtopic.service";
import { Router } from "@angular/router";
import { SubtopicService } from "src/app/services/subtopic.service";
import { MathSubTopic } from "src/app/services/models/subtopics";
import { UserService } from "src/app/services/user.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-math-topic",
  templateUrl: "./math-topic.component.html",
  styleUrls: ["./math-topic.component.css"]
})
export class MathTopicComponent implements OnInit {
  topic: MathTopic;
  mathSubtopic: MathSubTopic;
  mathSubtopics: MathSubTopic[] = [];
  mathTopics: MathTopic[] = [];

  public classes = ["5", "6"];

  constructor(
    private mathTopicService: MathtopicService,
    private router: Router,
    private mathSubtopicService: SubtopicService,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.topic = new MathTopic();
    this.mathSubtopic = new MathSubTopic();
    this.topic.subtopics = [];
  }

  ngOnInit() {
    this.addSubtopic();
  }

  goToFirstPage() {
    this.router.navigate([""]);
  }

  goToMenu() {
    this.router.navigate(["/menu"]);
  }

  goToAddTest() {
    this.router.navigate(["/test"]);
  }

  addSubtopic(): void {
    this.topic.subtopics.push({
      name: ""
    });
  }

  onSubmit(): void {
    this.mathTopicService.addMathTopic(this.topic).subscribe(data => {
      if (data) {
        this.toastr.success("Успешно записана тема", "Тема");
      }
    });
  }

  logout(): void {
    this.userService.logout(this.router);
  }
}
