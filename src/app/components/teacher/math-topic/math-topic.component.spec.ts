import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MathTopicComponent } from "./math-topic.component";

describe("MathTopicComponent", () => {
  let component: MathTopicComponent;
  let fixture: ComponentFixture<MathTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MathTopicComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MathTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
