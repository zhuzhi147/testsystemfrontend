import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-students-page",
  templateUrl: "./students-page.component.html",
  styleUrls: ["./students-page.component.scss"]
})
export class StudentsPageComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {}

  scrollToElement(element: HTMLElement) {
    console.log(element);
    element.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  logout(): void {
    this.userService.logout(this.router);
  }
}
