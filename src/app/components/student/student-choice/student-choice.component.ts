import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MathtopicService } from "src/app/services/mathtopic.service";
import { MathTopic } from "src/app/services/models/topics";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { MathSubTopic } from "src/app/services/models/subtopics";
import { SubtopicService } from "src/app/services/subtopic.service";
import { MathtestService } from "src/app/services/mathtest.service";
import { MathTest } from "src/app/services/models/mathtest";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-student-choice",
  templateUrl: "./student-choice.component.html",
  styleUrls: ["./student-choice.component.scss"]
})
export class StudentChoiceComponent implements OnInit {
  mathTopics$: Observable<MathTopic[]>;
  mathSubtopics$: Observable<MathSubTopic[]>;
  mathTests$: Observable<MathTest[]>;
  mathTestId: string;

  constructor(
    private mathTopicService: MathtopicService,
    private mathSubtopicService: SubtopicService,
    private mathTestService: MathtestService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.mathTopics$ = this.mathTopicService.getMathTopicsByClass(params.id);
    });
  }

  topicSelect(mathTopic: string): void {
    this.mathSubtopics$ = this.mathSubtopicService.getSubtopicByTopicId(
      mathTopic
    );
  }

  subtopicSelect(subtopicId: string): void {
    this.mathTests$ = this.mathTestService.getTests(subtopicId);
  }

  mathTestSelect(mathTestId: string): void {
    this.mathTestId = mathTestId;
  }

  goToTestPage() {
    this.router.navigateByUrl(`student-test/${this.mathTestId}`);
  }

  goToHomePage() {
    this.router.navigate(["/student"]);
  }

  logout(): void {
    this.userService.logout(this.router);
  }
}
