import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { StudentChoiceComponent } from "./student-choice.component";

describe("StudentChoiceComponent", () => {
  let component: StudentChoiceComponent;
  let fixture: ComponentFixture<StudentChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentChoiceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
