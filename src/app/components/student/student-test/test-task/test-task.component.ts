import { Component, OnInit, Input } from "@angular/core";
import { MathTask, CorrectAnswer } from "src/app/services/models/mathtask";
import { MathtaskService } from "src/app/services/mathtask.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { UserService } from "src/app/services/user.service";
import { Location } from "@angular/common";
import { TimerService } from "src/app/services/timer.service";

export interface FinalCorrectAnswer {
  id: number;
  text: string;
  answer: string;
}

@Component({
  selector: "app-test-task",
  templateUrl: "./test-task.component.html",
  styleUrls: ["./test-task.component.scss", "../student-test.component.scss"]
})
export class TestTaskComponent implements OnInit {
  public mathTestTask$: Observable<MathTask>;
  public correctAnswer$: Observable<CorrectAnswer>;

  public skip: number = 0;
  public testId: string;
  public minutes: string;
  public seconds: string;
  public testAnswers: FinalCorrectAnswer[] = [];
  public answer: string;
  public isFinished: boolean = false;

  constructor(
    private mathTaskService: MathtaskService,
    private router: ActivatedRoute,
    private route: Router,
    private userService: UserService,
    private timerService: TimerService,
    private location: Location
  ) {}

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.testId = params.id;
      this.mathTestTask$ = this.mathTaskService.getTasks(
        params.id,
        `${this.skip}`
      );
    });

    this.timerService.minutes$.subscribe(minutes => (this.minutes = minutes));

    this.timerService.seconds$.subscribe(seconds => (this.seconds = seconds));

    this.timerService.remainingSeconds().subscribe({
      complete: () => (this.isFinished = true)
    });
  }

  next(total: number, test: MathTask) {
    if (this.skip <= total) {
      this.skip += 1;
      this.mathTestTask$ = this.mathTaskService.getTasks(
        this.testId,
        `${this.skip}`
      );
      if (this.answer) {
        this.testAnswers.push({
          id: test.id,
          answer:
            test.correctAnswer.trim() === this.answer.trim()
              ? "Правилен"
              : "Грешен",
          text: test.task
        });
      }
      this.correctAnswer$ = null;
    } else {
      this.isFinished = false;
      this.testAnswers = JSON.parse(localStorage.getItem("testAnswers"));
    }
    localStorage.setItem("testAnswers", JSON.stringify(this.testAnswers));
  }

  previous() {
    if (this.skip > 0) {
      this.skip -= 1;
      this.mathTestTask$ = this.mathTaskService.getTasks(
        this.testId,
        `${this.skip}`
      );
      this.correctAnswer$ = null;
    }
  }

  correctAnswer(answer: string, mathTask: MathTask): void {
    this.answer = answer;
    this.correctAnswer$ = this.mathTaskService.checkAnswer(
      mathTask.id.toString().trim(),
      answer.trim()
    );
  }

  answersArray(data: string): string[] {
    return data.split(",");
  }

  coorectAnswersNumber(): number {
    return this.testAnswers.filter(f => f.answer === "Правилен").length;
  }

  goToHomePage() {
    this.location.back();
    localStorage.removeItem("testAnswers");
  }

  logout(): void {
    this.userService.logout(this.route);
  }
}
