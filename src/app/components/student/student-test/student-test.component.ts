import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver
} from "@angular/core";
import { TestTaskComponent } from "./test-task/test-task.component";

@Component({
  selector: "app-student-test",
  templateUrl: "./student-test.component.html",
  styleUrls: ["./student-test.component.scss"]
})
export class StudentTestComponent implements OnInit {
  ngOnInit() {}
}
