import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { LoginFormComponent } from "./components/login-form/login-form.component";
import { NewPasswordComponent } from "./components/new_password/new-password/new-password.component";
import { MathTestComponent } from "./components/teacher/math-test/math-test.component";
import { LeftsideComponent } from "./components/teacher/math-test/leftside/leftside.component";
import { RightsideComponent } from "./components/teacher/math-test/rightside/rightside.component";
import { MenuComponent } from "./components/teacher/menu/menu.component";
import { RegisterComponent } from "./components/register/register.component";
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { JwtModule } from "@auth0/angular-jwt";
import { FlexLayoutModule } from "@angular/flex-layout";
import { StudentChoiceComponent } from "./components/student/student-choice/student-choice.component";
import { StudentsPageComponent } from "./components/student/students-page/students-page.component";
import { StudentTestComponent } from "./components/student/student-test/student-test.component";
import { TestTaskComponent } from "./components/student/student-test/test-task/test-task.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";

export function tokenGetter() {
  return localStorage.getItem("access_token");
}

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    NewPasswordComponent,
    LoginFormComponent,
    MathTestComponent,
    LeftsideComponent,
    RightsideComponent,
    MenuComponent,
    RegisterComponent,
    StudentChoiceComponent,
    StudentsPageComponent,
    StudentTestComponent,
    TestTaskComponent
  ],
  imports: [
    FlexLayoutModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:3003"],
        blacklistedRoutes: ["http://localhost:3003/auth/login"]
      }
    }),
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [TestTaskComponent]
})
export class AppModule {}
