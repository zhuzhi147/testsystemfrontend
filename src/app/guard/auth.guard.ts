import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import jwtDecode from "jwt-decode";
import { User } from "../services/models/user/user";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const expectedRole = route.data.expectedRole;

    if (localStorage.getItem("access_token")) {
      const token = jwtDecode(localStorage.getItem("access_token")) as User;
      if (!token || token.type !== expectedRole) {
        return false;
      }
      return true;
    } else {
      this.router.navigate(["/login"], {
        queryParams: { returnUrl: state.url }
      });
      return false;
    }
  }
}
