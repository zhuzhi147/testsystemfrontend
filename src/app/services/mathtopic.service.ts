import { Injectable } from "@angular/core";
import { MathTopic } from "./models/topics";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { API_URL } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class MathtopicService {
  constructor(private http: HttpClient) {}

  addMathTopic(mathTopic: MathTopic): Observable<MathTopic> {
    return this.http.post<MathTopic>(`${API_URL.MATH_TOPIC}/add`, mathTopic);
  }

  getMathTopics(): Observable<MathTopic[]> {
    return this.http.get<MathTopic[]>(`${API_URL.MATH_TOPIC}/all`);
  }

  getMathTopicsByClass(classSelect: string): Observable<MathTopic[]> {
    const params = {
      class: classSelect
    };
    return this.http.get<MathTopic[]>(`${API_URL.MATH_TOPIC}/byClass`, {
      params: params
    });
  }
}
