import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { MathTask, CorrectAnswer } from "./models/mathtask";
import { API_URL } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class MathtaskService {
  constructor(private http: HttpClient) {}

  public getTasks(mathTestId: string, skip: string): Observable<MathTask> {
    const params = {
      params: new HttpParams().set("mathTestId", mathTestId).set("skip", skip)
    };
    return this.http.get<MathTask>(API_URL.MATH_TEST_TASK, params);
  }

  public checkAnswer(
    mathTaskId: string,
    answer: string
  ): Observable<CorrectAnswer> {
    const params = {
      params: new HttpParams()
        .set("mathTaskId", mathTaskId)
        .set("answer", answer)
    };

    return this.http
      .get<CorrectAnswer>(`${API_URL.MATH_TEST_TASK}/answer`, params)
      .pipe(map(answer => answer));
  }
}
