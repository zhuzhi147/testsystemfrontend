import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, timer, NEVER } from "rxjs";
import { switchMap, map, takeWhile, tap, startWith } from "rxjs/operators";

const K = 1000;
const INTERVAL = K;
const MINUTES = 5;
const TIME = MINUTES * K * 60;

let current: number;
let time = TIME;

const toMinutesDisplay = (ms: number) => Math.floor(ms / K / 60);
const toSecondsDisplay = (ms: number) => Math.floor(ms / K) % 60;

const toSecondsDisplayString = (ms: number) => {
  const seconds = toSecondsDisplay(ms);
  return seconds < 10 ? `0${seconds}` : seconds.toString();
};

const currentSeconds = () => time / INTERVAL;
const toMs = (t: number) => t * INTERVAL;
const toRemainingSeconds = (t: number) => currentSeconds() - t;

@Injectable({
  providedIn: "root"
})
export class TimerService {
  toggle$: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor() {}

  public remainingSeconds(): Observable<number> {
    return this.toggle$.pipe(
      switchMap((running: boolean) => (running ? timer(0, INTERVAL) : NEVER)),
      map(toRemainingSeconds),
      takeWhile(t => t >= 0)
    );
  }

  public ms$ = this.remainingSeconds().pipe(
    map(toMs),
    tap(t => (current = t))
  );

  public minutes$: Observable<string> = this.ms$.pipe(
    map(toMinutesDisplay),
    map(s => s.toString()),
    startWith(toMinutesDisplay(time).toString())
  );

  public seconds$: Observable<string> = this.ms$.pipe(
    map(toSecondsDisplayString),
    startWith(toSecondsDisplayString(time).toString())
  );
}
