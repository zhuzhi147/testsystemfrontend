import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { API_URL } from "src/environments/environment";
import { User } from "./models/user/user";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private http: HttpClient) {}

  public register(user: User): Observable<User> {
    return this.http.post<User>(`${API_URL.USER}/signup`, user);
  }

  public login(user: User): Observable<User> {
    return this.http.post<User>(`${API_URL.AUTH}/login`, user);
  }

  public logout(router: Router): void {
    localStorage.removeItem("access_token");
    router.navigateByUrl("login");
  }
}
