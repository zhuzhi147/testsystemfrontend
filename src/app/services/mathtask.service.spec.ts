import { TestBed } from '@angular/core/testing';

import { MathtaskService } from './mathtask.service';

describe('MathtaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MathtaskService = TestBed.get(MathtaskService);
    expect(service).toBeTruthy();
  });
});
