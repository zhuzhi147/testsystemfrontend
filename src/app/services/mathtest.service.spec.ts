import { TestBed } from '@angular/core/testing';

import { MathtestService } from './mathtest.service';

describe('MathtestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MathtestService = TestBed.get(MathtestService);
    expect(service).toBeTruthy();
  });
});
