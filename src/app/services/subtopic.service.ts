import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { MathSubTopic } from "./models/subtopics";
import { API_URL } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class SubtopicService {
  constructor(private http: HttpClient) {}

  getSubtopicByTopicId(id: string): Observable<MathSubTopic[]> {
    const params = {
      topicId: id
    };

    return this.http.get<MathSubTopic[]>(`${API_URL.MATH_SUBTOPIC}`, {
      params: params
    });
  }
}
