import { MathSubTopic } from "./subtopics";

export class MathTopic {
  id: number;
  name: string;
  subtopics: MathSubTopic[];
  class: number;
}
