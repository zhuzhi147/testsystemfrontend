export class Teacher {
  id: number;
  name: string;
  approved: boolean = false;
}
