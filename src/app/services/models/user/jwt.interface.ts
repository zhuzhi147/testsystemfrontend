export interface JwtPayload {
  id: number;
  accessToken: string;
  expiresIn: number;
}
