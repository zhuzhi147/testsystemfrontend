import { JwtPayload } from "./jwt.interface";
import { Teacher } from "./teacher";

export class User implements JwtPayload {
  accessToken: string;
  expiresIn: number;
  id: number;
  name: string;
  username: string;
  type: string;
  email: string;
  password: string;
  teacher?: Teacher;
}
