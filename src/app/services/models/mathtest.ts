import { MathTask } from "./mathtask";
import { MathSubTopic } from "./subtopics";

export class MathTest {
  id: string;
  name: string;
  mathSubTopic: MathSubTopic;
  mathTasks: MathTask[];
}
