export class MathTask {
  id?: number;
  image: string;
  answers: string;
  correctAnswer: string;
  task: string;
}

export interface CorrectAnswer {
  answer: boolean;
}
