import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { MathTest } from "./models/mathtest";
import { Observable } from "rxjs";
import { API_URL } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class MathtestService {
  constructor(private http: HttpClient) {}

  public createTest(mathTest: MathTest): Observable<MathTest> {
    return this.http.post<MathTest>(`${API_URL.MATH_TEST}`, mathTest);
  }

  public getTests(subtopicId: string): Observable<MathTest[]> {
    const params = {
      params: new HttpParams().set("subtopicId", subtopicId)
    };
    return this.http.get<MathTest[]>(`${API_URL.MATH_TEST}`, params);
  }
}
