import { TestBed } from '@angular/core/testing';

import { MathtopicService } from './mathtopic.service';

describe('MathtopicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MathtopicService = TestBed.get(MathtopicService);
    expect(service).toBeTruthy();
  });
});
