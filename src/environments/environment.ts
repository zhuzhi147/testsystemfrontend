// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const API_URL = {
  MATH_TOPIC: "http://192.168.43.240:3000/mathtopic",
  MATH_SUBTOPIC: "http://192.168.43.240:3000/mathsubtopic",
  MATH_TEST: "http://192.168.43.240:3000/mathtest",
  MATH_TEST_TASK: "http://192.168.43.240:3000/mathtesttask",
  USER: "http://192.168.43.240:3000/user",
  AUTH: "http://192.168.43.240:3000/auth"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
