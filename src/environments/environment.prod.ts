export const environment = {
  production: true
};

export const API_URL = {
  MATH_TOPIC: "http://192.168.43.240:3000/mathtopic",
  MATH_SUBTOPIC: "http://192.168.43.240:3000/mathsubtopic",
  MATH_TEST: "http://192.168.43.240:3000/mathtest",
  MATH_TEST_TASK: "http://192.168.43.240:3000/mathtesttask",
  USER: "http://192.168.43.240:3000/user",
  AUTH: "http://192.168.43.240:3000/auth"
};
